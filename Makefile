# Copyright (C) 2012 Sebastian Pipping <sebastian@pipping.org>
# Licensed under GPL v2 or later

all:

dist:
	rm -f MANIFEST
	./setup.py sdist

.PHONY: dist
