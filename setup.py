#!/usr/bin/env python
# Copyright (C) 2012 Sebastian Pipping <sebastian@pipping.org>
# Licensed under GPL v2 or later

from distutils.core import setup
from overlint.cli import VERSION_STR

setup(
	name='overlint',
	description='Simple tool for static analysis of Gentoo overlays',
	license='GPL v2 or later',
	version=VERSION_STR,
	url='http://git.overlays.gentoo.org/gitweb/?p=proj/overlint.git;a=summary',
	author='Sebastian Pipping',
	author_email='sebastian@pipping.org',
	packages=['overlint', ],
	scripts=['overlint-cli', ],
)
